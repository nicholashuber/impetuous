import datetime
import itertools
import unittest
import unittest.mock

import pytz

from impetuous.sheet import WhenArgType, WhenTime, Unit, UnitNotFoundError
from impetuous.jira import JIRAModule

utc = pytz.utc


class WhenArgTypeTestCase(unittest.TestCase):

    def setUp(self):
        self.now = datetime.datetime(2010, 5, 4, 16, 44, 36, tzinfo=utc)
        self.test_timezone = pytz.timezone('America/Vancouver')
        self.whentype = WhenArgType(utcnow=lambda: self.now, localtz=self.test_timezone)
        super().setUp()

    def test_bad_unit(self):
        with self.assertRaises(ValueError):
            self.whentype('+10z')

    def test_bad_order(self):
        with self.assertRaises(ValueError):
            self.whentype('+10s10m')

    def test_fixed_time(self):
        self.assertEqual(datetime.datetime(2010, 5, 4, 12, 0, 0, tzinfo=self.test_timezone),
                         self.whentype('12:00').absolute_datetime([]))

    def test_fixed_time_with_seconds(self):
        self.assertEqual(datetime.datetime(2010, 5, 4, 12, 1, 2, tzinfo=self.test_timezone),
                         self.whentype('12:01:02').absolute_datetime([]))

    def test_add_time(self):
        self.assertEqual(datetime.datetime(2010, 5, 4, 17, 54, 40, tzinfo=utc),
                         self.whentype('+1h10m4s').absolute_datetime([]))

    def test_dot_now_thingy(self):
        self.assertEqual(datetime.datetime(2010, 5, 4, 16, 34, 32, tzinfo=utc),
                         self.whentype('.-10m4s').absolute_datetime([]))

    def test_remove_time(self):
        self.assertEqual(datetime.datetime(2010, 5, 4, 16, 34, 32, tzinfo=utc),
                         self.whentype('-10m4s').absolute_datetime([]))

    def test_missing_sign(self):
        with self.assertRaises(ValueError):
            self.whentype('10m4s')

    def test_unit_seek_end_no_tasks(self):
        units = []
        with self.assertRaises(UnitNotFoundError):
            self.whentype(']').absolute_datetime(units)

    def test_unit_seek_end_with_tasks(self):
        units = [
            Unit(task='test', start=self.now.replace(minute=1), end=self.now.replace(minute=2)),
            Unit(task='test', start=self.now.replace(minute=3), end=self.now.replace(minute=4)),
        ]
        self.assertEqual(self.now.replace(minute=4), self.whentype(']').absolute_datetime(units))
        self.assertEqual(self.now.replace(minute=2), self.whentype(']2').absolute_datetime(units))

    def test_unit_seek_end_without_task_end(self):
        units = [
            Unit(task='test', start=self.now.replace(minute=1), end=self.now.replace(minute=2)),
            Unit(task='test', start=self.now.replace(minute=2)),
        ]
        with self.assertRaises(ValueError):
            self.whentype(']').absolute_datetime(units)

    def test_unit_relative_start(self):
        units = [
            Unit(task='test', start=self.now.replace(minute=1)),
            Unit(task='test', start=self.now.replace(minute=2)),
            Unit(task='test', start=self.now.replace(minute=3)),
        ]
        self.assertEqual(self.now.replace(minute=3), self.whentype('[').absolute_datetime(units))

    def test_unit_relative_start_count(self):
        units = [
            Unit(task='test', start=self.now.replace(minute=1)),
            Unit(task='test', start=self.now.replace(minute=2)),
            Unit(task='test', start=self.now.replace(minute=3)),
        ]
        self.assertEqual(self.now.replace(minute=1), self.whentype('[3').absolute_datetime(units))

    def test_unit_relative_compound_time(self):
        units = [
            Unit(task='test', start=self.now.replace(minute=1)),
            Unit(task='test', start=self.now.replace(minute=2)),
            Unit(task='test', start=self.now.replace(minute=3)),
        ]
        self.assertEqual(self.now.replace(minute=46), self.whentype('[+43m').absolute_datetime(units))


class JIRAPost(unittest.TestCase):

    def setUp(self):
        self.now = datetime.datetime(2010, 5, 4, 1, 0, 0, tzinfo=utc)
        self.module = JIRAModule()
        super().setUp()

    def test_comment(self):
        unit = Unit(
            task='test',
            start=self.now.replace(minute=0, second=0),
            end=self.now.replace(minute=1, second=30),
            comments=['Hello world'],
        )
        self.assertEqual(
            self.module.prepare_submission(unit, 'test')['comment'],
            'Hello world',
        )

    def test_comments(self):
        unit = Unit(
            task='test',
            start=self.now.replace(minute=0, second=0),
            end=self.now.replace(minute=1, second=30),
            comments=['foobar', 'spam', 'eggs'],
        )
        self.assertEqual(
            self.module.prepare_submission(unit, 'test')['comment'],
            '- foobar\n- spam\n- eggs',
        )

    def test_30_30(self):
        unit = Unit(
            task='test',
            start=self.now.replace(minute=0, second=0),
            end=self.now.replace(minute=1, second=30),
        )

        with unittest.mock.patch('random.random') as patch:
            patch.side_effect = (x / 100 for x in range(100)).__next__
            durations = [self.module.prepare_submission(unit, 'test')['timeSpentSeconds'] for _ in range(100)]

        avg = sum(durations) / len(durations)
        self.assertAlmostEqual(avg, 90.0)

    def test_10_50(self):
        unit = Unit(
            task='test',
            start=self.now.replace(minute=0, second=0),
            end=self.now.replace(minute=1, second=12),
        )

        with unittest.mock.patch('random.random') as patch:
            patch.side_effect = (x / 100 for x in range(100)).__next__
            durations = [self.module.prepare_submission(unit, 'test')['timeSpentSeconds'] for _ in range(100)]

        avg = sum(durations) / len(durations)
        self.assertAlmostEqual(avg, 72.0)


if __name__ == '__main__':
    unittest.main()
