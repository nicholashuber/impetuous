import configparser
import os


CONFIG_INI_PATH = os.path.expanduser('~/.config/impetuous/config.ini')


def get_config():
    config = configparser.ConfigParser()
    with open(CONFIG_INI_PATH) as fp:
        config.readfp(fp)
    return config
