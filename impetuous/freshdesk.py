import contextlib
import logging

from impetuous.ext import Submission, SubmissionStatus, LudicrousConditions
from impetuous.ext import PatternSubmissionDiscovery


logger = logging.getLogger(__name__)


class FreshdeskSubmission(dict, Submission):

    def __init__(self, ticket_id, **kwargs):
        self.ticket_id = ticket_id
        super().__init__(kwargs)

    @property
    def label(self):
        return self.ticket_id

    def status(self, unit):
        status = super().status(unit)
        if status is SubmissionStatus.unsubmitted:
            if self.label in unit.post_results.get('freshdesk', {}):
                return SubmissionStatus.submitted
            else:
                return SubmissionStatus.unsubmitted
        else:
            return status

    def submit(self, server, api_key):
        import requests
        url = server + '/api/v2/tickets/{}/time_entries'.format(self.ticket_id)
        logger.debug("Logging time to freshdesk at %s.", url)
        resp = requests.post(url, auth=(api_key, 'X'), json=self)
        try:
            resp.raise_for_status()
        except requests.exceptions.HTTPError as e:
            raise LudicrousConditions(e, resp.content)
        else:
            return resp.json()

    def update_submitted_unit(self, unit, result):
        unit_results = unit.post_results.setdefault('freshdesk', {})
        assert self.ticket_id not in unit_results, ('%r not expected in %r' % (self.ticket_id, unit_results))
        unit_results[self.ticket_id] = result


class Freshdesk(PatternSubmissionDiscovery):

    @contextlib.contextmanager
    def discovery_context(self, impetuous):
        try:
            pattern = impetuous.get_config_or_empty()['freshdesk']['pattern']
        except KeyError as e:
            raise LudicrousConditions(e, 'Try editing the config file and set `freshdesk.pattern`.')
        yield {
            'pattern': pattern,
        }

    @contextlib.contextmanager
    def submission_context(self, impetuous):
        config = impetuous.get_config_or_empty()
        try:
            yield {
                'server': config['freshdesk']['server'],
                'api_key': config['freshdesk']['api_key'],
            }
        except KeyError as e:
            raise LudicrousConditions(e, 'Try editing the config file and set `freshdesk.server` and `freshdesk.api_key`.')

    def prepare_submission(self, unit, match):
        # There doesn't seem to be an obvious way to post a time entry for the
        # past, so every entry looks like it is for the present time I guess...
        time_spent = '%02i:%02i' % divmod(unit.duration_in_minutes, 60)
        return FreshdeskSubmission(
            ticket_id=match,
            time_spent=time_spent,
            note=unit.full_comment,
            timer_running=not 'if I have anything to do with it',
        )
