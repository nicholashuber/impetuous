from fnmatch import fnmatch
import contextlib
import datetime
import itertools
import logging
import math
import os
import pprint
import subprocess

import colorama as ca

from impetuous.config import CONFIG_INI_PATH
from impetuous.sheet import (
    SheetDirectory, Sheet, Unit,
    SheetNotFoundError, UnitNotFoundError, LinkNotFoundError,
    SHEET_DIR, CURRENT_SHEET_NAME,
    utcnow, localtz,
)
from impetuous.config import get_config
from impetuous.ext import get_modules, LudicrousConditions, SubmissionStatus


logger = logging.getLogger(__name__)


def maybe_elide(string, max_length):
    assert max_length > 3
    if len(string) > max_length:
        chunklen = (max_length - 3) // 2
        return string[:chunklen] + '...' + string[-chunklen:]
    else:
        return string


class Impetuous():

    def __init__(self):
        try:
            self.config = get_config()
        except FileNotFoundError as e:
            logger.debug('Config could not be loaded: %r', e)
            self.config = None
            self.config_error = e
        else:
            self.config_error = None

    def new_sheet(self):
        return Sheet()

    def load_sheet(self, directory=SHEET_DIR, name=CURRENT_SHEET_NAME):
        return SheetDirectory(directory).load_by_name(name)

    def save_sheet(self, sheet, directory=SHEET_DIR, name=CURRENT_SHEET_NAME):
        return SheetDirectory(directory).save(sheet, name)

    def get_config_or_none(self):
        return self.config

    def get_config_or_empty(self):
        if self.config is None:
            return {}
        else:
            return self.config

    def get_config_or_quit(self):
        if self.config_error is not None:
            logger.error(self.config_error)
            logger.error('Try creating that file, see the readme for some ideas of what to put in it.')
            raise SystemExit(1)
        else:
            return self.config

    def get_modules(self):
        return get_modules()

    def get_module_by_link_name(self, name):
        for module in get_modules():
            if module.link_name == name:
                return module
        raise KeyError('No module with link name %r' % name)

    def get_unit_submissions(self, sheet, *units):
        """
        Returns a list of module, unit, submission tuples.
        """
        for module in self.get_modules():
            try:
                with module.discovery_context(self) as context:
                    for unit in units:
                        for submission in module.discover(unit, **context):
                            logger.debug("%s found submission %r.", type(module).__name__, submission)
                            yield module, unit, submission
            except LudicrousConditions as e:
                logger.warning("%s could not discover submissions: %s. %s", type(module).__name__, e.error, e.suggestion)


class ImpetuousCli(Impetuous):

    def add_module_arguments(self, parser):
        for module in self.get_modules():
            parser.add_argument(
                '--%s' % module.link_name,
                '-%s' % module.link_shortcut,
                nargs='*',
            )

    def print_unit_submissions(self, unit, verbose=False):
        config = self.get_config_or_none()
        if config is not None:
            modules = get_modules()
            for module in modules:
                try:
                    with module.discovery_context(self) as context:
                        for submission in module.discover(unit, **context):
                            status = submission.status(unit)
                            print(' ', end='')
                            print(module.shortcut, end='')
                            if verbose:
                                print('[', end='')
                            if status is SubmissionStatus.invalid:
                                print(ca.Style.BRIGHT, ca.Fore.RED, '!', ca.Style.RESET_ALL, sep='', end='')
                            elif status is SubmissionStatus.unsubmitted:
                                print(ca.Style.BRIGHT, '*', ca.Style.RESET_ALL, sep='', end='')
                            else:
                                pass
                            if verbose:
                                print(submission.label, end=']')
                except LudicrousConditions as e:
                    logger.debug("%s could not discover submissions: %s. %s", type(module).__name__, e.error, e.suggestion)
                    continue

    def print_unit(self, unit, verbose=True):
        self.print_unit_start(unit, end=' ')
        self.print_unit_duration(unit, end=' ')
        self.print_unit_end(unit, end=' ')
        self.print_unit_task(unit, end='')
        if verbose:
            self.print_unit_submissions(unit, verbose=verbose)
            print()
            if unit.comments:
                for comment in unit.comments:
                    print(' - %s' % comment)
        else:
            if unit.comments:
                comment_length = max(10, 50 // len(unit.comments))
                comments = (maybe_elide(comment, comment_length) for comment in unit.comments)
                print(' (%s)' % '; '.join(comments), end='')
            self.print_unit_submissions(unit, verbose=verbose)
            print()

    def print_unit_task(self, unit, **kwargs):
        if unit.covert:
            print('(', unit.task, ')', sep='', **kwargs)
        else:
            print(ca.Style.BRIGHT, unit.task, ca.Style.RESET_ALL, sep='', **kwargs)

    def print_unit_start(self, unit, **kwargs):
        print(ca.Style.BRIGHT, ca.Fore.GREEN, self.format_datetime(unit.start), ca.Style.RESET_ALL, sep='', **kwargs)

    def print_unit_end(self, unit, **kwargs):
        print(ca.Fore.GREEN, self.format_datetime(unit.end), ca.Fore.RESET, sep='', **kwargs)

    def format_datetime(self, dt):
        if dt is None:
            return '--:--:--'
        else:
            dt = dt.astimezone(localtz)
            is_today = dt.date() == utcnow().astimezone(localtz).date()
            if is_today:
                return dt.time().strftime('%H:%M:%S')
            else:
                return str(dt)

    def print_unit_duration(self, unit, **kwargs):
        self.print_timedelta(unit.duration, **kwargs)

    def print_timedelta(self, delta, **kwargs):
        print(ca.Fore.BLUE, ca.Style.BRIGHT, self.format_timedelta(delta), ca.Style.RESET_ALL, ca.Fore.RESET, sep='', **kwargs)

    def format_timedelta(self, delta):
        seconds = int(delta.total_seconds())
        if seconds < 0:
            negate = True
            seconds = abs(seconds)
        else:
            negate = False
        minutes, seconds = divmod(seconds, 60)
        hours, minutes = divmod(minutes, 60)
        return '{}{:}:{:02}:{:02}'.format('-' if negate else '', hours, minutes, seconds)


def action_summary(impetuous, args):
    sheet = impetuous.load_sheet()
    keyfnc = lambda unit: unit.task
    summary_units = sorted((unit for unit in sheet.units if not unit.covert), key=keyfnc)
    for task, units in itertools.groupby(summary_units, keyfnc):
        total = sum((unit.duration for unit in units), datetime.timedelta(0))
        # This is awkward
        start = utcnow()
        unit = Unit(task=task, start=start, end=start+total)
        impetuous.print_unit_duration(unit, end=' ')
        impetuous.print_unit_task(unit)
    grand_total = sum((unit.duration for unit in summary_units), datetime.timedelta(0))
    start = utcnow()
    impetuous.print_unit_duration(Unit(task='', start=start, end=start+grand_total))


def action_log(impetuous, args):
    sheet = impetuous.load_sheet()
    units = sorted(sheet.units, key=lambda unit: unit.start, reverse=True)
    for unit, next_unit in zip(units, [None] + units[:-1]):
        if next_unit is not None:
            if unit.end is None:
                # This should not happen
                raise NotImplementedError
            else:
                # There is a following unit, and this unit has an end, so I
                # guess draw the end and the gap to the next unit.
                gap = next_unit.start - unit.end
                if gap != datetime.timedelta(seconds=0):
                    print(ca.Fore.MAGENTA, '(', impetuous.format_timedelta(gap), ')', ca.Style.RESET_ALL, sep='')
        impetuous.print_unit(unit, verbose=args.verbose)


def action_start(impetuous, args):
    start_task = args.what
    if start_task == '-':
        sheet = impetuous.load_sheet()
        unit = sheet.get_last_ended_unit()
        if unit is None:
            raise UnitNotFoundError()
        start_task = unit.task

    try:
        action_done(impetuous, args)
    except (SheetNotFoundError, UnitNotFoundError):
        pass

    try:
        sheet = impetuous.load_sheet()
    except SheetNotFoundError:
        sheet = impetuous.new_sheet()
    logger.debug(sheet)

    start_when = args.when.absolute_datetime(sheet.units)

    unit = sheet.new_unit(start=start_when, task=start_task, covert=args.covert, comments=args.comments)
    unit.validate()
    impetuous.save_sheet(sheet)

    print('Started ...')
    impetuous.print_unit(unit)


def action_comment(impetuous, args):
    sheet = impetuous.load_sheet()
    unit = sheet.last_unit
    if any(submission.status(unit) is SubmissionStatus.submission
           for _, submission in impetuous.unit_submissions(unit)):
        logger.warning('Modifying a unit with posted submissions.')
    unit.comments.append(' '.join(args.words))
    sheet.validate()
    impetuous.save_sheet(sheet)
    impetuous.print_unit(unit, verbose=True)


def action_done(impetuous, args):
    sheet = impetuous.load_sheet()
    unit = sheet.last_unit
    if unit.end is None:
        unit.end = args.when.absolute_datetime(sheet.units)
        sheet.validate()
        impetuous.save_sheet(sheet)

        print('Finished ...')
        impetuous.print_unit(unit)
    else:
        raise UnitNotFoundError('The last unit is not in progress.')


def action_edit(impetuous, args):
    editor = os.environ['EDITOR']
    filepath = os.path.join(SHEET_DIR, CURRENT_SHEET_NAME)
    subprocess.check_call([editor, filepath])


def action_config_edit(impetuous, args):
    editor = os.environ['EDITOR']
    filepath = CONFIG_INI_PATH
    subprocess.check_call([editor, filepath])


def action_rename(impetuous, args):
    sheet = impetuous.load_sheet()
    if not sheet.units:
        raise UnitNotFoundError('The current sheet is empty.')
    else:
        if args.old is None:
            old_task = sheet.units[-1].task
        else:
            old_task = args.old
        changed = False
        for unit in sheet.units:
            if unit.task == old_task:
                print('Renaming ', end='')
                impetuous.print_unit_start(unit, end=' ')
                impetuous.print_unit_task(unit, end=' ')
                print('to ', end='')
                unit.task = args.new
                impetuous.print_unit_task(unit)
                impetuous.print_unit(unit)
                changed = True
        if not changed:
            print('Nothing to do.')
        else:
            impetuous.save_sheet(sheet)


class FakeSubmissionResult(object):

    def __init__(self, submission):
        pass


def action_post(impetuous, args):
    config = impetuous.get_config_or_quit()
    sheet = impetuous.load_sheet()
    units = [
        unit for unit in sheet.units
        if any(fnmatch(unit.task, pattern) for pattern in args.units)
    ]
    if not units:
        print('No matching units!')
    else:
        submission_map = {}  # {unit: {module: submission[]}}
        for (module, unit, submission) in impetuous.get_unit_submissions(sheet, *units):
            if submission.status(unit) is SubmissionStatus.unsubmitted:
                logger.debug("%s found submitable submissions %r.", type(module).__name__, submission)
                submission_map.setdefault(unit, {}).setdefault(module, []).append(submission)

        module_submission_contexts = {}
        with contextlib.ExitStack() as stack:
            for unit, module_map in submission_map.items():
                for module, submissions in module_map.items():
                    for submission in submissions:
                        if args.dry_run:
                            result = FakeSubmissionResult(submission)
                        else:
                            try:
                                context = module_submission_contexts[module]
                            except KeyError:
                                try:
                                    context = stack.enter_context(module.submission_context(impetuous))
                                except LudicrousConditions as e:
                                    logger.error("%s failed to open submission context: %s. %s", type(module).__name__, e.error, e.suggestion)
                                    continue
                                else:
                                    module_submission_contexts[module] = context
                            try:
                                result = submission.submit(**context)
                            except LudicrousConditions as e:
                                logger.error("%s failed to submit %r: %s. %s", type(module).__name__, submission, e.error, e.suggestion)
                                continue
                        logger.debug('Submitted %r with result %r', submission, result)
                        submission.update_submitted_unit(unit, result)
                        if not args.dry_run:
                            impetuous.save_sheet(sheet)
                impetuous.print_unit(unit, verbose=True)


def _action_link_base(impetuous, args):

    config = impetuous.get_config_or_quit()
    sheet = impetuous.load_sheet()

    if args.units:
        task_matches = args.units
    else:
        task_matches = [sheet.last_unit.task]

    unit_links = [(unit, sheet.unit_links(unit)) for unit in sheet.units]

    for module in impetuous.get_modules():
        for link in getattr(args, module.link_name) or []:
            for task_match in task_matches:
                yield (sheet, task_match, module, link)

    impetuous.save_sheet(sheet)

    for unit, old_links in unit_links:
        links = sheet.unit_links(unit)
        if links != old_links:
            impetuous.print_unit(unit)


def action_link(impetuous, args):
    for (sheet, task_match, module, link) in _action_link_base(impetuous, args):
        logger.debug('Linking %s tasks from %s %s', task_match, module.name, link)
        sheet.add_link(task_match, module, link)


def action_unlink(impetuous, args):
    for (sheet, task_match, module, link) in _action_link_base(impetuous, args):
        logger.debug('Unlinking %s tasks from %s %s', task_match, module.name, link)
        try:
            sheet.remove_link(task_match, module, link)
        except LinkNotFoundError as e:
            logger.error(e)
            raise SystemExit(1)

    return
    units = [
        unit for unit in sheet.units
        if any(fnmatch(unit.task, pattern) for pattern in args.units)
    ]
    if not units:
        print('No matching units!')
    else:
        for module in impetuous.get_modules():
            thing = getattr(args, module.link_name)
            if thing is not None:
                with module.discovery_context(impetuous) as context:
                    module.discover(**context)
