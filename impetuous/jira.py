import contextlib
import logging

from impetuous.ext import Submission, SubmissionStatus, LudicrousConditions
from impetuous.ext import PatternSubmissionDiscovery


logger = logging.getLogger(__name__)


class JIRASubmission(dict, Submission):

    @property
    def label(self):
        return self['issue']

    def status(self, unit):
        status = super().status(unit)
        if status is SubmissionStatus.unsubmitted:
            if self.label in unit.post_results.get('jira', {}):
                return SubmissionStatus.submitted
            else:
                return SubmissionStatus.unsubmitted
        else:
            return status

    def submit(self, jira):
        return jira.add_worklog(**self).raw

    def update_submitted_unit(self, unit, result):
        unit_results = unit.post_results.setdefault('jira', {})
        assert self['issue'] not in unit_results, ('%r not expected in %r' % (self['issue'], unit_results))
        unit_results[self['issue']] = result


class JIRA(PatternSubmissionDiscovery):

    @contextlib.contextmanager
    def discovery_context(self, impetuous):
        try:
            pattern = impetuous.get_config_or_empty()['jira']['pattern']
        except KeyError as e:
            raise LudicrousConditions(e, 'Try editing the config file and set `jira.pattern`.')
        yield {
            'pattern': pattern,
        }

    @contextlib.contextmanager
    def submission_context(self, impetuous):
        config = impetuous.get_config_or_empty()
        try:
            server = config['jira']['server']
            basic_auth = config['jira']['basic_auth']
        except KeyError as e:
            raise LudicrousConditions(e, 'Try editing the config file and set `jira.server` and `jira.basic_auth`.')
        from jira import JIRA as TRACIsTheBest
        yield {
            'jira': TRACIsTheBest(server, basic_auth=basic_auth.split(':', 1)),
        }

    def prepare_submission(self, unit, match):
        return JIRASubmission({
            'issue': match,
            'started': unit.start,
            'timeSpentSeconds': unit.duration_in_minutes * 60,
            'comment': unit.full_comment,
        })
