from fnmatch import fnmatch
import functools
import datetime
import itertools
import logging
import math
import os
import random
import re
import time

import dateutil.parser
import yaml
try:
    from yaml import (
        CLoader as yaml_Loader,
        CDumper as yaml_Dumper,
    )
except ImportError:
    from yaml import (
        Loader as yaml_Loader,
        Dumper as yaml_Dumper,
    )


logger = logging.getLogger('impetuous')

localtz = dateutil.tz.tzlocal()


def utcnow():
    return datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc)


SHEET_DIR = os.path.expanduser('~/.config/impetuous/sheets')
CURRENT_SHEET_FMT = os.environ.get('IM_SHEET_FMT', '{local_time:%Y-%m-%d}')
CURRENT_SHEET_NAME = CURRENT_SHEET_FMT.format(
    local_time=utcnow().astimezone(localtz)
)


class WhenTime(object):

    class UnitSeek(object):

        class StartEdge:
            pass

        class EndEdge:
            pass

        def __init__(self, count, edge):
            self.count = count
            self.edge = edge

    def __init__(self, initial, seek=None, delta=None):
        assert initial.tzinfo is not None
        self.initial = initial
        self.seek = seek
        self.delta = delta

    def absolute_datetime(self, units):
        value = self.initial
        if self.seek is not None:
            steps = []
            for unit in units:
                if self.seek.edge is self.seek.StartEdge:
                    time = unit.start
                else:
                    time = unit.end
                if time is None:
                    raise ValueError("Refusing seek past null time")
                elif time < value:
                    steps.append(time)
                else:
                    break
            try:
                value = steps[-self.seek.count]
            except IndexError:
                raise UnitNotFoundError("No unit matching seek")
        if self.delta is not None:
            value += self.delta
        return value


class WhenArgType(object):

    when_format_re = re.compile(
        # Fixed initial
        r'((\.|now)|(?:(\d{1,2}):(\d{1,2}):?(\d{1,2})?))?'
        # Seek
        r'(([[\]])(\d*))?'
        # Relative offset
        r'(([+-])(\d+h)?(\d+m)?(\d+s)?)?'
        r'$'
    )

    def __init__(self, utcnow=utcnow, localtz=localtz):
        self.utcnow = utcnow
        self.localtz = localtz

    def __repr__(self):
        """ For formatting nicely when displayed in an argument parsing message.
        """
        return 'WhenArgType'

    def __call__(self, text):
        """
        Returns a WhenTime which can be used to produce an absolute time given
        the context of a collection of units.
        """
        if text == 'now':
            return WhenTime(self.utcnow())

        match = self.when_format_re.match(text)
        if match is not None:
            (initial, initial_dot, initial_hour, initial_minute, initial_second,
             seek, seek_edge, seek_count,
             delta, delta_sign, delta_hours, delta_minutes, delta_seconds
             ) = match.groups()

            # Set initial
            if initial is None or initial_dot:
                initial = self.utcnow()
            else:
                time = datetime.time(hour=int(initial_hour),
                                     minute=int(initial_minute),
                                     second=0 if initial_second is None else int(initial_second[0:]))
                today = self.utcnow().astimezone(self.localtz).date()
                initial = datetime.datetime.combine(today, time).replace(tzinfo=self.localtz)

            # Set seek
            if seek is not None:
                if seek_edge == '[':
                    seek_edge = WhenTime.UnitSeek.StartEdge
                else:
                    seek_edge = WhenTime.UnitSeek.EndEdge
                seek = WhenTime.UnitSeek(edge=seek_edge,
                                         count=int(seek_count) if seek_count else 1)

            # Finally make relative/delta time adjustment
            if delta is not None:
                delta = datetime.timedelta(
                    hours=0 if delta_hours is None else int(delta_hours[:-1]),
                    minutes=0 if delta_minutes is None else int(delta_minutes[:-1]),
                    seconds=0 if delta_seconds is None else int(delta_seconds[:-1])
                )
                if delta_sign == '-':
                    delta = -delta

            return WhenTime(initial=initial, seek=seek, delta=delta)
        else:
            raise ValueError("Bad format '{}'.".format(text[0:]))


class UnitNotFoundError(RuntimeError):
    pass


class SheetNotFoundError(RuntimeError):
    pass


class LinkNotFoundError(RuntimeError):
    pass


class ValidationError(Exception):
    pass


class Unit(object):

    def __init__(self, start, task, end=None, covert=False, post_results=None, comments=None):
        assert start.tzinfo is not None
        if end is not None:
            assert end.tzinfo is not None
        self.start = start
        self.task = task
        self.end = end
        self.covert = covert
        self.post_results = post_results or {}
        self.comments = [] if comments is None else comments

    def validate(self):
        if self.end is not None and self.end < self.start:
            raise ValidationError("Unit cannot end at {} before it begins at {}.".format(self.end, self.start))

    @property
    def duration(self):
        if self.end is None:
            end = utcnow()
        else:
            end = self.end
        return end - self.start

    @property
    def duration_in_minutes(self):
        """ Return duration in minutes as integer. Rounds randomly. :)
        """
        minutes_spent = self.duration.total_seconds() / 60
        if minutes_spent % 1 <= random.random():
            return math.floor(minutes_spent)
        else:
            return math.ceil(minutes_spent)

    @property
    def full_comment(self):
        if len(self.comments) == 1:
            return self.comments[0]
        else:
            return '\n'.join('- %s' % comment for comment in self.comments)

    @classmethod
    def yaml_representer(cls, dumper, unit):
        as_dict = {
            'start': unit.start.astimezone(localtz),
            'task': unit.task,
        }
        if unit.end is not None:
            as_dict['end'] = unit.end.astimezone(localtz)
        if unit.covert:
            as_dict['covert'] = True
        if unit.post_results:
            as_dict['post_results'] = unit.post_results
        if unit.comments:
            as_dict['comments'] = unit.comments
        return dumper.represent_mapping(u'!unit', as_dict)

    @classmethod
    def yaml_constructor(cls, loader, node):
        return cls(**loader.construct_mapping(node, deep=True))


class ModuleLinks(dict):

    def unit_links(self, unit):
        return list(itertools.chain.from_iterable(
            values
            for pattern, values in self.items()
            if fnmatch(unit.task, pattern)
        ))

    def link_pattern(self, pattern, value):
        self.setdefault(pattern, set()).add(value)

    def unlink_pattern(self, pattern, value):
        links = self[pattern]
        links.remove(value)
        if not links:
            del self[pattern]


class Sheet(object):

    def __init__(self, links=None, units=None):
        self.links = links or {}
        self.units = units or []

    def new_unit(self, *args, **kwargs):
        new_unit = Unit(*args, **kwargs)
        self.units.append(new_unit)
        return new_unit

    def add_link(self, pattern, module, value):
        try:
            module_links = self.links[module.link_name]
        except KeyError:
            module_links = ModuleLinks()
            self.links[module.link_name] = module_links
        module_links.link_pattern(pattern, value)

    def remove_link(self, pattern, module, value):
        try:
            module_links = self.links[module.link_name]
            module_links.unlink_pattern(pattern, value)
        except KeyError:
            raise LinkNotFoundError("No link to %s found using pattern %r for %s" % (value, pattern, module.name))
        if not module_links:
            del self.links[module.link_name]

    def validate(self):
        for unit in self.units:
            unit.validate()

    @property
    def last_unit(self):
        if self.units:
            return self.units[-1]
        else:
            raise UnitNotFoundError('The current sheet is empty.')

    def get_last_ended_unit(self):
        units = filter(lambda unit: unit.end is not None, self.units)
        if units:
            return sorted(units, key=lambda unit: unit.end, reverse=True)[0]

    @classmethod
    def yaml_representer(cls, dumper, sheet):
        return dumper.represent_mapping(u'!sheet', {
            'links': sheet.links,
            'units': sheet.units,
        })

    @classmethod
    def yaml_constructor(cls, loader, node):
        return cls(**loader.construct_mapping(node, deep=True))


class SheetDirectory(object):

    def __init__(self, path=SHEET_DIR):
        self.path = path
        # Will this even work? This is bananas...
        class SheetDirectory_yaml_Dumper(yaml_Dumper):
            pass
        class SheetDirectory_yaml_Loader(yaml_Loader):
            pass
        self._yaml_dumper = SheetDirectory_yaml_Dumper
        self._yaml_loader = SheetDirectory_yaml_Loader
        self._yaml_dumper.add_representer(Unit, Unit.yaml_representer)
        self._yaml_dumper.add_representer(Sheet, Sheet.yaml_representer)
        self._yaml_loader.add_constructor(u'!unit', Unit.yaml_constructor)
        self._yaml_loader.add_constructor(u'!sheet', Sheet.yaml_constructor)
        self._yaml_loader.add_constructor(u'tag:yaml.org,2002:timestamp', timestamp_constructor)

    def save(self, sheet, name):
        path = os.path.join(self.path, name)
        logger.debug("Saving to %s", path)
        os.makedirs(os.path.dirname(path), exist_ok=True)
        with open(path + '~', 'w') as file:
            yaml.dump(sheet, file, indent=4, default_flow_style=False, Dumper=self._yaml_dumper)
        os.rename(path + '~', path)

    def load(self, file):
        sheet = yaml.load(file, Loader=self._yaml_loader)
        if sheet is None:
            raise SheetNotFoundError('Sheet exists on filesystem but is empty')
        return sheet

    def load_by_name(self, name):
        try:
            with open(os.path.join(self.path, name), 'r') as file:
                return self.load(file)
        except FileNotFoundError as e:
            raise SheetNotFoundError(e)


# http://stackoverflow.com/a/13295663
def timestamp_constructor(loader, node):
    return dateutil.parser.parse(node.value)
