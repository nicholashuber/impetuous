#!/usr/bin/env python3

from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
import logging

import colorama as ca

from impetuous.sheet import SheetNotFoundError, UnitNotFoundError, WhenArgType
from impetuous.cli import (
    ImpetuousCli,
    action_log, action_start, action_done, action_edit, action_post,
    action_config_edit, action_rename, action_summary, action_comment,
)


logger = logging.getLogger(__name__)


def main():
    ca.init()

    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter, add_help=False)
    parser.add_argument('-l', '--log',
                        help='Logging level',
                        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR'],
                        default='WARNING')

    args, remaining_args = parser.parse_known_args()

    # Recreate the parser without add_help. We want the help thingy, but we
    # don't want to catch '-h' when parsing the very first time. TODO XXX FIXME
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('-l', '--log',
                        help='Logging level',
                        choices=['DEBUG', 'INFO', 'WARNING', 'ERROR'],
                        default='WARNING')

    logging.basicConfig(level=getattr(logging, args.log))
    impetuous = ImpetuousCli()

    subparser = parser.add_subparsers(title='action')
    subparser.required = True
    subparser.dest = 'the thing it is that you are trying to do'

    log_help = '''Show a list things done, startings, endings, and durations
    for the current sheet.'''
    log_args = subparser.add_parser('log', help=log_help, aliases=['l'], formatter_class=ArgumentDefaultsHelpFormatter)
    log_args.add_argument('--verbose', '-v', action='store_true', default=False)
    log_args.set_defaults(action=action_log)

    #link_help = ''''''
    #link_args = subparser.add_parser('link', help=link_help, aliases=['k'], formatter_class=ArgumentDefaultsHelpFormatter)
    #link_args.add_argument('units', type=str, help='', nargs='*', default=None)
    #impetuous.add_module_arguments(link_args)
    #link_args.set_defaults(action=action_link)

    #unlink_help = ''''''
    #unlink_args = subparser.add_parser('unlink', help=unlink_help, aliases=['u'], formatter_class=ArgumentDefaultsHelpFormatter)
    #unlink_args.add_argument('units', type=str, help='', nargs='*', default=None)
    #impetuous.add_module_arguments(unlink_args)
    #unlink_args.set_defaults(action=action_unlink)

    summary_help = '''Show a summary of total time spent on things in the
    current sheet.'''
    summary_args = subparser.add_parser('summary', help=summary_help, aliases=['ll'], formatter_class=ArgumentDefaultsHelpFormatter)
    summary_args.set_defaults(action=action_summary)

    start_help = '''Mark the start of doing a new thing, stopping any current
    things if applicable.'''
    start_args = subparser.add_parser('start', help=start_help, aliases=['s'], formatter_class=ArgumentDefaultsHelpFormatter)
    start_args.add_argument('what', type=str)
    start_args.add_argument('when', type=WhenArgType(), nargs='?', default='now', help='Start of period')
    start_args.add_argument('--covert', action='store_true', default=False, help='Covert entries do not show up in summaries (they do show up in the log) and are never posted.')
    start_args.add_argument('--comments', '-c', type=str, nargs='*', help='')
    #impetuous.add_module_arguments(start_args)
    start_args.set_defaults(action=action_start)

    comment_help = '''Add a comment to a thing being done.'''
    comment_args = subparser.add_parser('comment', help=comment_help, aliases=['c'], formatter_class=ArgumentDefaultsHelpFormatter)
    comment_args.add_argument('words', type=str, nargs='+')
    comment_args.set_defaults(action=action_comment)

    done_help = '''Mark the ending of the current thing being done.'''
    done_args = subparser.add_parser('done', help=done_help, aliases=['d'], formatter_class=ArgumentDefaultsHelpFormatter)
    done_args.add_argument('when', type=WhenArgType(), nargs='?', default='now')
    done_args.set_defaults(action=action_done)

    rename_help = '''Renames every thing done in this sheet called <old> to
    <new>, <old> defaults to what is being done currently.'''
    rename_args = subparser.add_parser('rename', help=rename_help, aliases=['r'], formatter_class=ArgumentDefaultsHelpFormatter)
    rename_args.add_argument('old', nargs='?', default=None)
    rename_args.add_argument('new')
    #rename_args.add_argument('-a', '--all', help='Rename all units that match')
    rename_args.set_defaults(action=action_rename)

    edit_help = '''Opens the current sheet in EDITOR.'''
    edit_args = subparser.add_parser('edit', help=edit_help)
    edit_args.set_defaults(action=action_edit)

    config_help = '''Opens the config in EDITOR.'''
    config_args = subparser.add_parser('config-edit', help=config_help)
    config_args.set_defaults(action=action_config_edit)

    post_help = '''Submits worklogs to JIRA and Freshdesk.'''
    post_args = subparser.add_parser('post', help=post_help, formatter_class=ArgumentDefaultsHelpFormatter)
    post_args.add_argument('units', type=str, nargs='*', help='Submit all vaild units with names match the glob.', default='*')
    post_args.add_argument('-n', '--dry_run', action='store_true', default=False, help='Do not actually send submissions, just talk about them.')
    post_args.set_defaults(action=action_post)

    args = parser.parse_args(remaining_args)
    action = args.action
    try:
        action(impetuous, args)
    except (SheetNotFoundError, UnitNotFoundError) as e:
        logger.error(e)


if __name__ == '__main__':
    main()
