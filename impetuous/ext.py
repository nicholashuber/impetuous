from abc import ABC, abstractmethod
from enum import Enum
import logging
import re


logger = logging.getLogger(__name__)


def get_modules():
    from impetuous.jira import JIRA
    from impetuous.freshdesk import Freshdesk
    return (JIRA(), Freshdesk())


class LudicrousConditions(RuntimeError):
    """
    Raise when when the module can't complete a request due invalid state like
    incomplete configuration or the weather or something. The program may
    display your error and quit or ignore it.
    """

    def __init__(self, error, suggestion):
        self.error = error
        self.suggestion = suggestion


class SubmissionStatus(Enum):

    invalid = 0
    unsubmitted = 1
    submitted = 2


class Submission(ABC):

    def status(self, unit) -> SubmissionStatus:
        if unit.covert:
            return SubmissionStatus.invalid
        elif unit.end is None:
            return SubmissionStatus.invalid
        elif unit.duration.total_seconds() <= 0:
            return SubmissionStatus.invalid
        elif unit.duration.total_seconds() < 60:
            logger.warning("%s has fewer than 60 seconds of time logged. JIRA/things will lose their mind I try to log this.", unit.task)
            return SubmissionStatus.invalid
        else:
            return SubmissionStatus.unsubmitted

    @abstractmethod
    def submit(self, **kwargs):
        """
        Return some sort of something that does whatever
        """

    @abstractmethod
    def update_submitted_unit(self, unit, submission, result):
        """
        Modify the given unit such that the status() know that it is submitted
        if it were called after this.

        result may be a FakeSubmissionResult if a dry run thingy is going on.
        """


class Module(ABC):
    """
    Modules allow for talking to external APIs so you can post time to JIRA or
    Freshdesk or whatever.

    They shouldn't be too slow, as they may be created and run frequently, such
    as whenever a unit is printed.
    """

    @property
    def shortcut(self):
        """Single character to identify the module."""
        return self.name[0]

    @property
    def name(self):
        """Short but readable label to identify the module."""
        return type(self).__name__

    @property
    def link_shortcut(self):
        return self.shortcut.lower()

    @property
    def link_name(self):
        return self.name.lower()

    @abstractmethod
    def discovery_context(self, impetuous):
        """
        Return a context manager that returns a dictionary when entered. The
        dictionary is passed to discover() as extra keyword arguments.

        Raise LudicrousConditions if a discovery_context cannot be provided for
        some reason.
        """

    @abstractmethod
    def discover(self, unit, **kwargs):
        """
        yield Submission objects. **kwargs are from the context manager
        returned by discovery_context().
        """

    @abstractmethod
    def submission_context(self, impetuous):
        """
        Return a context manager that returns a dictionary when entered. The
        dictionary is passed to Submission.submit() as extra keyword arguments.
        """


class LinkSubmissionDiscovery(Module):

    def discover(self, unit, pattern):
        pass


class PatternSubmissionDiscovery(Module):
    """
    A base class for modules that get submissions from matching regular
    expressions on the unit name.

    Requires that the discovery_context context manager returns a dictionary
    with the key 'pattern' when entered.
    """

    def discover(self, unit, pattern):
        logger.debug('%s looking for matches in %r using pattern %s.', type(self).__name__, unit.task, pattern)
        for match in re.findall(pattern, unit.task):
            logger.debug('%s found %r in %r using pattern %s.', type(self).__name__, match, unit.task, pattern)
            submission = self.prepare_submission(unit, match)
            if submission is not None:
                yield submission

    @abstractmethod
    def prepare_submission(self, unit, match):
        """
        Return a Submission for the unit and match
        """
