Impetuous
=========

.. image:: https://img.shields.io/badge/build-disappointed-blue.svg
.. image:: https://img.shields.io/badge/coverage-no-lightgrey.svg
.. image:: https://img.shields.io/badge/downloads-743M%2Fday-brightgreen.svg
.. image:: https://img.shields.io/badge/node.js-webscale-orange.svg
.. image:: https://img.shields.io/badge/badge-trendy-green.svg

This is some time/task tracking software. 🐑

It can talk (barely) to/at JIRA.

Requirements
------------

python 3.4 (or 3.5 just to be safe)

Installation
------------

Clone the source code and install with :code:`python3 -m pip install --user -e .[jira,freshdesk]`. If you want. :sup:`You don't have to.` :sub:`I'm not the police.`

CLI Usage
---------

Sheets live in the directory :code:`~/.config/impetuous/sheets`. They are in the YAML file format.

The "current sheet" is some file in the sheet directory. The name of the sheet that is considered the "current" one follows the format in the environment variable :code:`IM_SHEET_FMT` and defaults to :code:`{local_time:%Y-%m-%d}`. The format string is formatted with the local time using python's :code:`str.format()` function such that, by default, impetuous will use a different sheet each day.

A number of commands take a :code:`when` argument. This has a number of acceptable formats. I can't explain it since it's over-engineered and stupid. Here are some examples:

- :code:`14:15` - 2:15 P.M.
- :code:`.+1h5s` - One hour and five seconds after now.
- :code:`.-10m` - Ten minutes before now.
- :code:`]+90s` - Ninety seconds after the end of the last/current task.
- :code:`[+1h5s` - One hour and five seconds after the beginning of the last/current task.

Limitations
-----------

Don't start tasks before the last one ended or something might break.

Configuration and JIRA and Freskdesk
------------------------------------

Edit the configuration by running :code:`im config-edit`, which just opens the configuration file in :code:`~/.config/impetuous/config.ini` in :code:`EDITOR`. This is an example :code:`config.ini`::

    [jira]
    server = https://funkymonkey.atlassian.net
    basic_auth = admin:hunter2
    pattern = ((?:FOO)|(?:BAR)-\d+)

    [freshdesk]
    server = https://funkymonkey.freshdesk.com
    api_key = xxxxxxxxxxxxxxxxxxxx
    pattern = freskdesk #(\d+)

I don't think OAuth works for JIRA yet but it probably ought to.

Tests
-----

Oh man, I don't know. Just run :code:`nosetests` and hope for the best I suppose.
